const router = require('express').Router();
const mongoose = require('mongoose');
const Box = require('../model.js');

router.get('/',(req,res)=>{
    res.render('create',{title:'Create a new Document'});
});

router.get('/execute',(req,res)=>{
  var myGlobalObject = req.query;
  res.render('form-submit',{entity:JSON.stringify(myGlobalObject), title:'Form Submitted'});
  //console.log(req.url);
  const entity = new Box({
    _id : new mongoose.Types.ObjectId(),
    entityname : req.query.SongName,
    entitybody : req.query.Lyrics
  });
  entity.save().then(result=>{
        console.log(result);
    }).catch(err=>{
        console.log(err);
    });
});

module.exports = router;
