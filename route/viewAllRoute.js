const router= require('express').Router();
const mongoose = require('mongoose');
const Box = require('../model.js');

router.get('/',(req,res)=>{
    Box.find().select('-__v').sort('-date').exec().then(result=>{
      console.log(result);
        res.render('viewAll',{doc:JSON.stringify(result), size:'sizeOfArray(doc)',title:"All Documents"});

    })
    .catch()
})
router.get('/viewIndividial/:entityid',(req,res)=>{
  var entityid = req.params.entityid;
  Box.findById(entityid)
  .select('-__v')
  .exec()
  .then(result=>{
    res.render('viewIndividial',{doc:result, title:result.entityname});
    //console.log(result);
  })
  .catch()
  /*res.send(entityid);*/
})


module.exports = router;
