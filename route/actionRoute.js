const router = require('express').Router();
const mongoose = require('mongoose');
const Box = require('../model.js');
router.get('/edit/:id',(req,res)=>{
  console.log('got an PATCH request for id: '+req.params.id);
  res.render('uc');
});

router.get('/delete/:id',(req,res)=>{
  console.log('got an delete request for id: ' +req.params.id);
  var entityId = req.params.id;
  Box.deleteOne({_id:entityId}).exec().then().catch()
  res.redirect('/view');
});

module.exports = router;
