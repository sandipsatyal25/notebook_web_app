const express = require('express');
const createRoute = require('./route/createRoute');
const viewAllRoute = require('./route/viewAllRoute');
const actionRoute = require('./route/actionRoute');
const staticRoutes = require('./route/static');
const mongoose = require('mongoose');
const app = express();
app.set('view engine', 'ejs');
mongoose.connect('mongodb://admindaking:aiX2-k3@ds147420.mlab.com:47420/random-today',{useNewUrlParser: true},()=>{
  var db=mongoose.connection;
  console.log('Sucessfully connected to database');
  db.on('error',console.error.bind(console, 'connection error:'));
});

app.use('/resources',express.static('resources/'));
app.use('/view/viewIndividial/resources/', express.static('resources/'));
app.use('/action/edit/delete/',express.static('resources/'));
app.get('/',(req,res)=>{
res.render('home',{title:'Home'});
});
app.use('/create', createRoute);
app.use('/view', viewAllRoute);
app.use('/action', actionRoute);
app.listen('3000',()=>{
  console.log('running');
})
