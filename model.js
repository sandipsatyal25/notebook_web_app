const mongoose = require('mongoose');
const boxSchema = mongoose.Schema({
  _id:mongoose.Schema.Types.ObjectId,
  entityname: {type:String, required:true},
  entitybody:{type:String, required:true},
  date: { type: Date, default: Date.now }
});
module.exports = mongoose.model('Box', boxSchema);
